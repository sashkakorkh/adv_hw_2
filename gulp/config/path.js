import * as nodePath from 'path'

const rootFolder = nodePath.basename(nodePath.resolve())

const buildFolder = `./dist`;
const srcFolder = `./src`
const htmlFolder = `./`

export const path = {
    build: {
        js:`${buildFolder}/script/`,
        css: `${buildFolder}/css`,
        html:`${htmlFolder}/`,
        images:`${buildFolder}/images/`,
        fonts:`${buildFolder}/fonts`,

    },
    src: {

        js:`${srcFolder}/script/script.js`,
        scss: `${srcFolder}/styles/*.scss`,
        html: `${srcFolder}/*.html`,
        images: `${srcFolder}/images/**/*.{jpg,jpeg,png,gif}`,
        svg:`${srcFolder}/images/**/*.svg`,
    },
    watch: {
        js:`${srcFolder}/script/**/*.js`,
        scss: `${srcFolder}/styles/**/*.scss`,
        html: `${srcFolder}/**/*.html`,
        images: `${srcFolder}/images/**/*.{jpg,jpeg,png,gif,svg,ico}`
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder
}